package biblioteca.begin;

import biblioteca.control.BibliotecaCtrl;
import biblioteca.repository.repoInterfaces.CartiRepoInterface;
import biblioteca.repository.repoMock.CartiRepoMock;
import biblioteca.view.Consola;

import java.io.IOException;

//functionalitati
//i.	 adaugarea unei noi carti (titlu, autori, an aparitie, editura, cuvinte cheie);
//ii.	 cautarea cartilor scartiRepoise de un anumit autor (sau parti din numele autorului);
//iii.	 afisarea cartilor din biblioteca care au aparut intr-un anumit an, ordonate alfabetic dupa titlu si autori.



public class Start {
	
	public static void main(String[] args) {
		CartiRepoInterface cartiRepoInterface = new CartiRepoMock();
		BibliotecaCtrl bibliotecaCtrl = new BibliotecaCtrl(cartiRepoInterface);
		Consola consola = new Consola(bibliotecaCtrl);
		try {
			consola.executa();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
//	CartiRepoInterface cartiRepo = new CartiRepo();
//	BibliotecaCtrl bibliotecaCtrl = new BibliotecaCtrl(cartiRepo);
//	
//	Carte c = new Carte();
//	bibliotecaCtrl = new BibliotecaCtrl(cartiRepo);
//	c = new Carte();
//	
//	List<String> autori = new ArrayList<String>();
//	autori.add("Mateiu Caragiale");
//	
//	List<String> cuvinteCheie = new ArrayList<String>();
//	cuvinteCheie.add("mateiu");
//	cuvinteCheie.add("cartiRepoailor");
//	
//	c.setTitlu("Intampinarea cartiRepoailor");
//	c.setAutori(autori);
//	c.setAnAparitie("1948");
//	c.setEditura("Litera");
//	c.setCuvinteCheie(cuvinteCheie);
//	
//	
//	try {
//		for(Carte ca:bibliotecaCtrl.getCartiOrdonateDinAnul("1948"))
//			System.out.println(ca);
//	} catch (Exception e) {
//		// TODO Auto-generated catch block
//		e.printStackTrace();
//	}
//
//	
////	try {
////		bibliotecaCtrl.adaugaCarte(c);
////	} catch (Exception e) {
////		// TODO Auto-generated catch block
////		e.printStackTrace();
////	}
	
}
