package biblioteca;

import biblioteca.control.BibliotecaCtrl;
import biblioteca.model.Carte;
import biblioteca.repository.repo.CartiRepo;
import biblioteca.repository.repoInterfaces.CartiRepoInterface;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class BBT_tests {

	private CartiRepoInterface repo;
	private BibliotecaCtrl ctrl;
	private Carte carte;
	private List<String> autori;
	private List<String> cuvinte;
	
	@Before
	public void setUp() throws Exception {repo=new CartiRepo();
	ctrl=new BibliotecaCtrl(repo);
	autori=new ArrayList<String>();
	cuvinte= new ArrayList<String>();
	autori.add("Fratii Grimm");
	cuvinte.add("keyword");
	carte=new Carte();
	carte.setTitlu("Cenusareasa");
	carte.setAnAparitie("aaa");
	carte.setCuvinteCheie(cuvinte);
	carte.setEditura("Corint");
	carte.setReferenti(autori);
	}

	//TC10
	@Test
	public void testCuvantNotEmpty() throws Exception {
		setUp();
		cuvinte.add("");
		carte.setCuvinteCheie(cuvinte);
		try
		{
			ctrl.adaugaCarte(carte);
		}
		catch(Exception re)
		{
			String message = "cuvant cheie invalid!";
			assertEquals(message, re.getMessage());

		}
	}
	
	@Test
	public void testAutorNotEmpty() throws Exception {
		setUp();
		autori.add("");
		carte.setCuvinteCheie(autori);
		try
		{
			ctrl.adaugaCarte(carte);
		}
		catch(Exception re)
		{
			String message = "Autor invalid!";
			assertEquals(message, re.getMessage());

		}
	}

	//TC6
	@Test
	public void testAn() throws Exception {
		setUp();
		carte.setAnAparitie("1900");

		try
		{
			ctrl.adaugaCarte(carte);
		}
		catch(Exception re)
		{
			String message = "An aparitie invalid!";
			assertEquals(message, re.getMessage());

		}
	}
	
	@Test
	public void nonValidAnLow() throws Exception {
		setUp();
		
		carte.setAnAparitie("1900");

		try
		{
			ctrl.adaugaCarte(carte);
		}
		catch(Exception re)
		{
			String message = "An aparitie invalid!";
			assertEquals(message, re.getMessage());

		}
	}

	//TC8
	@Test
	public void nonValidAnUp() throws Exception {
		setUp();
		carte.setAnAparitie("2300");

		try
		{
			ctrl.adaugaCarte(carte);
		}
		catch(Exception re)
		{
			String message = "An aparitie invalid!";
			assertEquals(message, re.getMessage());

		}
	}

	//TC4
	@Test
	public void testEdituraString() throws Exception {
		setUp();
		carte.setEditura("123a");

		try
		{
			ctrl.adaugaCarte(carte);
		}
		catch(Exception re)
		{
			String message = "Editura Invalida";
			assertEquals(message, re.getMessage());

		}
	}
}
