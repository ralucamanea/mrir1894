package biblioteca;

import biblioteca.control.BibliotecaCtrl;
import biblioteca.model.Carte;
import biblioteca.repository.repoInterfaces.CartiRepoInterface;
import biblioteca.repository.repoMock.CartiRepoMock;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static junit.framework.TestCase.assertEquals;

/**
 * Created by raluc on 4/21/2018.
 */
public class Unit_Cerinta3_BBT {
    private CartiRepoInterface repo;
    private BibliotecaCtrl ctrl;
    private Carte carte;
    private List<String> autori;
    private List<String> cuvinte;

    @Before
    public void setUp() throws Exception {
        repo=new CartiRepoMock();
        ctrl=new BibliotecaCtrl(repo);
    }


    @Test
    // an valid, 1948, verific daca le ordoneaza asc
    public void testFiltrareCartiValid() throws Exception {
        String an="1948";
        List<Carte> carti=ctrl.getCartiOrdonateDinAnul(an);
        assertEquals(3, carti.size());
        assertEquals("Dale carnavalului", carti.get(0).getTitlu());
        assertEquals("Enigma Otiliei", carti.get(1).getTitlu());
        assertEquals("Intampinarea crailor", carti.get(2).getTitlu());
    }

    @Test
    //An invalid - 2200
    public void testFiltrareCartiInvalid() throws Exception {
        String an="2200";
        try
        {
            ctrl.getCartiOrdonateDinAnul(an);
        }
        catch(Exception re)
        {
            String message = "Nu e an valid!";
            Assert.assertEquals(message, re.getMessage());

        }
    }
}
