package biblioteca;

import biblioteca.control.BibliotecaCtrl;
import biblioteca.model.Carte;
import biblioteca.repository.repo.CartiRepo;
import biblioteca.repository.repoInterfaces.CartiRepoInterface;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import static org.junit.Assert.assertEquals;


public class BVA_valid {
	private CartiRepoInterface repo;
	private BibliotecaCtrl ctrl;
	private Carte carte;
	private List<String> autori;
	private List<String> cuvinte;
	
	@Before
	public void setUp() throws Exception {
		repo=new CartiRepo();
		ctrl=new BibliotecaCtrl(repo);
		autori=new ArrayList<String>();
		cuvinte= new ArrayList<String>();
		autori.add("Fratii Grimm");
		cuvinte.add("keyword");
		carte=new Carte();
		carte.setTitlu("Cenusareasa");
		carte.setAnAparitie("1901");
		carte.setCuvinteCheie(cuvinte);
		carte.setEditura("Corint");
		carte.setReferenti(autori);
		
	}

	//TC7
	@Test
	public void test() throws Exception {
		int size=ctrl.getCarti().size();
		try {
			ctrl.adaugaCarte(carte);
			assertEquals(size + 1, ctrl.getCarti().size());
		}catch (Exception ex){

		}
	}

}
