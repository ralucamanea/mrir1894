package biblioteca;

import biblioteca.control.BibliotecaCtrl;
import biblioteca.model.Carte;
import biblioteca.repository.repoInterfaces.CartiRepoInterface;
import biblioteca.repository.repoMock.CartiRepoMock;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by raluc on 4/22/2018.
 */
public class WBT_nonValid {
    private CartiRepoInterface repo;
    private BibliotecaCtrl ctrl;
    private Carte carte;
    private List<String> autori;
    private List<String> cuvinte;

    @Before
    public void setUp() throws Exception {
        repo=new CartiRepoMock();
        ctrl=new BibliotecaCtrl(repo);
    }

    @Test
    public void testCautaCarteNonValid() throws Exception {
        try {
            List<Carte> carti = ctrl.cautaCarte("ab1");

        } catch(Exception re)
        {
            String message = "String invalid";
            assertEquals(message, re.getMessage());

        }
    }

}
