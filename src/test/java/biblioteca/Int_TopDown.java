package biblioteca;

import biblioteca.control.BibliotecaCtrl;
import biblioteca.model.Carte;
import biblioteca.repository.repo.CartiRepo;
import biblioteca.repository.repoInterfaces.CartiRepoInterface;
import biblioteca.repository.repoMock.CartiRepoMock;
import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by raluc on 5/2/2018.
 */
public class Int_TopDown {

    private CartiRepoInterface repo;
    private CartiRepoInterface repoMock;
    private BibliotecaCtrl ctrl;
    private Carte carte;
    private List<String> autori;
    private List<String> cuvinte;

    @Before
    public void setUp() throws Exception {
        repoMock = new CartiRepoMock();
        repo = new CartiRepo();
        ctrl = new BibliotecaCtrl(repo);
    }

    //unit test pentru cerinta I
    @Test
    public void testA() throws Exception {
        Carte carte = Carte.getCarteFromString("Dale carnavalului;Caragiale;1948;Litera;caragiale,carnaval");
        int size = repoMock.getCarti().size();
        repoMock.adaugaCarte(carte);
        assertEquals(size + 1, repoMock.getCarti().size());
    }

    //integrare B
    @Test
    public void test_int_B() throws Exception{
        Carte carte = Carte.getCarteFromString("Dale carnavalului;Caragiale Ion;1948;Litera;caragiale,carnaval");
        int size = ctrl.getCarti().size();
        int sizeCautateInit = ctrl.cautaCarte("Ion").size();
        try {
            //A
            ctrl.adaugaCarte(carte);
            //B
            List<Carte> carti = ctrl.cautaCarte("Ion");

            assertEquals(size + 1, ctrl.getCarti().size());
            assertEquals(sizeCautateInit + 1, carti.size());

        } catch (Exception ex) {
            ex.printStackTrace();

        }
    }

    //integrare C
    @Test
    public void test_int_C() throws Exception{
        Carte carte = Carte.getCarteFromString("Dale carnavalului;Caragiale Ion;1948;Litera;caragiale,carnaval");
        int size = ctrl.getCarti().size();
        int sizeCautateInit = ctrl.cautaCarte("Ion").size();
        int sizeCartiOrdonate = ctrl.getCartiOrdonateDinAnul("1948").size();
        try {
            //A
            ctrl.adaugaCarte(carte);
            //B
            List<Carte> carti = ctrl.cautaCarte("Ion");
            //C
            List<Carte> cartiOrdonate = ctrl.getCartiOrdonateDinAnul("1948");

            assertEquals(size + 1, ctrl.getCarti().size());
            assertEquals(sizeCautateInit + 1, carti.size());
            TestCase.assertEquals(sizeCartiOrdonate+1, cartiOrdonate.size());

        } catch (Exception ex) {
            ex.printStackTrace();

        }
    }
}
