package biblioteca;

import biblioteca.control.BibliotecaCtrl;
import biblioteca.model.Carte;
import biblioteca.repository.repoInterfaces.CartiRepoInterface;
import biblioteca.repository.repoMock.CartiRepoMock;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by raluc on 4/22/2018.
 */
public class WBT_tests {
    private CartiRepoInterface repo;
    private BibliotecaCtrl ctrl;
    private Carte carte;
    private List<String> autori;
    private List<String> cuvinte;

    @Before
    public void setUp() throws Exception {
        repo=new CartiRepoMock();
        ctrl=new BibliotecaCtrl(repo);
    }

    @Test
    public void testCautaCarteValid() throws Exception {
        setUp();
        List<Carte> carti=repo.cautaCarte("cara");
        assertEquals(3,carti.size());
        assertEquals("Povesti", carti.get(0).getTitlu());
    }

    @Test
    public void testCautaCarteNonValid() throws Exception {
        setUp();
        try {
            List<Carte> carti = ctrl.cautaCarte("");

        } catch(Exception re)
        {
            String message = "String invalid";
            assertEquals(message, re.getMessage());

        }
    }

    @Test
    public void testCautaCarte3() throws Exception {
        setUp();
        List<Carte> carti=repo.cautaCarte("line");
        assertEquals(2,carti.size());
        assertEquals("Enigma Otiliei", carti.get(0).getTitlu());
    }

    @Test
    public void testCautaCarte4() throws Exception {
        setUp();
        List<Carte> carti=repo.cautaCarte("testttt");
        assertEquals(0,carti.size());
    }

    @Test
    public void testCautaCarteEmptyAutor() throws Exception {
        setUp();
        repo.deleteAll();
        Carte carte=Carte.getCarteFromString("Poezii;;1973;Corint;poezii");
        repo.adaugaCarte(carte);
        List<Carte> carti=repo.cautaCarte("z");
        assertEquals(0,carti.size());

    }


    //TC01
    @Test
    public void testCautaCarte6() throws Exception {
        setUp();
        repo.deleteAll();
        List<Carte> carti=repo.cautaCarte("b");
        assertEquals(0,carti.size());

    }

    @Test
    public void testCautaCarte7() throws Exception {
        setUp();
        List<Carte> carti=repo.cautaCarte("mihAI");
        assertEquals(1,carti.size());
        assertEquals("Povesti", carti.get(0).getTitlu());
    }
}
