package biblioteca;

import biblioteca.control.BibliotecaCtrl;
import biblioteca.model.Carte;
import biblioteca.repository.repoInterfaces.CartiRepoInterface;
import biblioteca.repository.repoMock.CartiRepoMock;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by raluc on 4/22/2018.
 */
public class WBT_valid {
    private CartiRepoInterface repo;
    private BibliotecaCtrl ctrl;
    private Carte carte;
    private List<String> autori;
    private List<String> cuvinte;

    @Before
    public void setUp() throws Exception {
        repo=new CartiRepoMock();
        ctrl=new BibliotecaCtrl(repo);
    }

    @Test
    public void testCautaCarteValid() throws Exception {
        List<Carte> carti=repo.cautaCarte("Ion");
        assertEquals(2,carti.size());
        assertEquals("Povesti", carti.get(0).getTitlu());
    }



}
